# İyzico - React Dashboard Challenge

### Technologies
- React
- WebPack
- Css Modules
- EcmaScript 6-7

### How to Install ?
```sh
$ git clone https://ozkilic@bitbucket.org/ozkilic/react-dashboard.git
$ cd react-dashboard
$ npm install
```

### Development Build
```
$ npm start
$ open http://localhost:3000
```

### Production Build
```
$ npm run build
$ npm install -g pushstate-server
$ pushstate-server build
$ open http://localhost:9000
```
