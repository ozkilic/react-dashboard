import React, { Component } from 'react';
import './styles/main.css';

import TwoColumnsLayout from './layouts/TwoColumns';
import SideBar from './containers/SideBar';
import HomeView from './containers/HomeView';
import Footer from './components/Footer';

const sidebar = React.createElement(SideBar);
const homeview = React.createElement(HomeView);
const footer = React.createElement(Footer);

class app extends Component {
  render() {
    return (
      <div className="wrapper">
        <TwoColumnsLayout main={homeview} sidebar={sidebar} footer={footer} />
      </div>
    );
  }
}

export default app;
