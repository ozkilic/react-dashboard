import React, { PropTypes } from 'react';
import {Icon} from 'react-fa';
import defaultStyles from './styles.css';

function Footer({ styles }) {
  return (
    <div className={styles.footer}>
      <ul>
        <li>
          <span>Copyright  M7amadsa3ed 2015. All rights reserved</span>
        </li>

        <li>
          <span>Careers</span>
        </li>

        <li>
          <span>Privacy Policy</span>
        </li>

        <li className={styles.lastItem}>
          <span>
            Feedback
            <Icon name="caret-down" className={styles.icon} />
          </span>
        </li>
      </ul>
    </div>
  );
}

Footer.defaultProps = {
  styles: defaultStyles,
};

Footer.propTypes = {
  styles: PropTypes.object,
};

export default Footer;
