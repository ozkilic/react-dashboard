import React, { PropTypes } from 'react';
import defaultStyles from './styles.css';
import { Icon } from 'react-fa'

import logo1x from './logo.png';
import logo2x from './logo@2x.png';

function Header({ styles }) {
  return (
    <div className={styles.header}>
      <div className={styles.leftSide}>
        <img
          src={logo1x}
          srcSet={`${logo1x} 1x, ${logo2x} 2x`}
          role="presentation"
          className={styles.logo}
          />
      </div>

      <div className={styles.rightSide}>
        <div className={styles.container}>
          <div className={styles.head}>
            <h2 className={styles.title}>Dashboard</h2>
          </div>
          <div className={styles.menu}>
            <Icon name="bell" size="2x" className={styles.icon} />
            <Icon name="envelope" size="2x" className={styles.icon} />
            <Icon name="search" size="2x" className={styles.icon} />
            <Icon name="th" size="2x" className={styles.icon} />
          </div>
        </div>
      </div>
    </div>
  );
}

Header.defaultProps = {
  styles: defaultStyles,
}

Header.propTypes = {
  styles: PropTypes.object,
}

export default Header;
