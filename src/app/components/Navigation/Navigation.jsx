import React, { PropTypes } from 'react';
import classNames from 'classnames';
import {Icon} from 'react-fa';
import defaultStyles from './styles.css';


function Navigation(props) {
  const {
    styles,
  } = props;
  return (
    <div className={styles.navigation}>
      <ul className={styles.navList}>
        <li className={styles.navItem}>
          <div className={styles.iconWrap}>
            <Icon name="home" className={styles.icon} />
          </div>
          <span>Dashboard</span>
          <span className={classNames(styles.count, styles.red)}>3</span>
        </li>

        <li className={styles.navItem}>
          <div className={styles.iconWrap}>
            <Icon name="flash" className={styles.icon} />
          </div>
          <span>Apps</span>
          <span className={classNames(styles.count, styles.green)}>9</span>
        </li>

        <li className={styles.navItem}>
          <div className={styles.iconWrap}>
            <Icon name="lock" className={styles.icon} />
          </div>
          <span>Components</span>
          <span className={classNames(styles.count, styles.blue)}>4</span>
        </li>

        <li className={styles.navItem}>
          <div className={styles.iconWrap}>
            <Icon name="envelope" className={styles.icon} />
          </div>
          <span>EMAIL</span>
        </li>
      </ul>

      <ul className={styles.navList}>
        <li className={styles.navItem}>
          <div className={styles.iconWrap}>
            <Icon spin name="cog" className={styles.icon} />
          </div>
          <span>Settings</span>
        </li>

        <li className={styles.navItem}>
          <div className={styles.iconWrap}>
            <Icon name="file-text" className={styles.icon} />
          </div>
          <span>Document</span>
          <span className={classNames(styles.count, styles.blue)}>2</span>
        </li>
      </ul>
    </div>
  );
}

Navigation.defaultProps = {
  styles: defaultStyles,
};

Navigation.propTypes = {
  styles: PropTypes.object,
};

export default Navigation;
