import React, { PropTypes } from 'react';
import defaultStyles from './styles.css';

function RectangleBox(props) {
  const {
    styles,
    title,
    subTitle,
    count,
  } = props;
  return (
    <div className={styles.box}>
      <div className={styles.left}>
        <h1 className={styles.title}>{title}</h1>
        <h3 className={styles.subTitle}>{subTitle}</h3>
      </div>

      <div className={styles.right}>
        <span className={styles.count}>{count}</span>
      </div>
    </div>
  );
}

RectangleBox.defaultProps = {
  styles: defaultStyles,
}

RectangleBox.propTypes = {
  styles: PropTypes.object,
  title: PropTypes.string,
  subTitle: PropTypes.string,
  count: PropTypes.number,
}

export default RectangleBox;
