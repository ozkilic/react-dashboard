import React, { PropTypes } from 'react';
import classNames from 'classnames';
import defaultStyles from './styles.css';

function SmallBox(props) {
  const {
    styles,
    count,
    title,
    color,
    bgcolor,
    icon,
    chart,
  } = props;

  const countStyle = classNames(styles.count, color);
  const iconStyle = classNames(styles.icon, icon);
  const bottomStyle = classNames(styles.bottom, bgcolor);
  const chartStyle = classNames(styles.chart, chart);

  return (
    <div className={styles.box}>
      <div className={styles.row}>
        <div className={styles.left}>
          <h1 className={countStyle}>{count}</h1>
          <h3 className={styles.title}>{title}</h3>
        </div>

        <div className={styles.right}>
          <span className={iconStyle}></span>
        </div>
      </div>

      <div className={bottomStyle}>
        <div className={styles.label}>% change</div>
        <div className={chartStyle}></div>
      </div>
    </div>
  );
}

SmallBox.defaultProps = {
  styles: defaultStyles,
}

SmallBox.propTypes = {
  styles: PropTypes.object,
  count: PropTypes.number,
  title: PropTypes.string,
  color: PropTypes.string,
  bgcolor: PropTypes.string,
  icon: PropTypes.string,
}

export default SmallBox;
