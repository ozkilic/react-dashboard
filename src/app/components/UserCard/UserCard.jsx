import React, { PropTypes } from 'react';
import defaultStyles from './styles.css';

import {Icon} from 'react-fa';

function UserCard(props) {
  const {
    name,
    surname,
    title,
    photo,
    styles,
  } = props;

  return (
    <div className={styles.userCard}>
      <div className={styles.row}>
        <div className={styles.left}>
          <img src={photo} alt={name+surname} className={styles.photo} />
        </div>

        <div className={styles.right}>
          <h3 className={styles.name}>{`${name} ${surname}`}</h3>
          <span className={styles.title}>
            {title}
            <Icon name="caret-down" className={styles.icon} />
          </span>
        </div>
      </div>
    </div>
  );
}

UserCard.defaultProps = {
  styles: defaultStyles,
}

UserCard.propTypes = {
  name: PropTypes.string,
  surname: PropTypes.string,
  title: PropTypes.string,
  photo: PropTypes.string,
  styles: PropTypes.object,
};

export default UserCard;
