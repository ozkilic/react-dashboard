import React, { PropTypes } from 'react';
import defaultStyles from './styles.css';
import Header from '../../components/Header';

function HeaderContainer({ styles }) {
  return (
    <div>
      <Header />
    </div>
  );
}

HeaderContainer.defaultProps = {
  styles: defaultStyles,
}

HeaderContainer.propTypes = {
  styles: PropTypes.object,
}

export default HeaderContainer;
