import React, { PropTypes } from 'react';
import defaultStyles from './styles.css';
import RectangleBox from '../../components/RectangleBox';
import SmallBox from '../../components/SmallBox';

const baseData = [
  {
    title: "Welcome to Infinity",
    subTitle: "Number Of Views",
    count: 102,
  },
  {
    title: "Active",
    subTitle: "Leads/Contacts",
    count: 325,
  }
];

function HomeView({ styles }) {
  return (
    <div className={styles.homeView}>
      <div className={styles.row}>
        {baseData.map((data, index) => {
          return (
            <RectangleBox {...data} key={index} />
          );
        })}
      <div className={styles.clearfix}></div>
      </div>

      <div className={styles.row}>
        <SmallBox
          count={66.163}
          title='Total Leads'
          color={styles.blue}
          bgcolor={styles.bgblue}
          icon={styles.icon1}
          chart={styles.chart}
        />

        <SmallBox
          count={3.490}
          title='Total Pending'
          color={styles.red}
          bgcolor={styles.bgred}
          icon={styles.icon2}
          chart={styles.chart}
        />

        <SmallBox
          count={8.372}
          title='Case Closed'
          color={styles.green}
          bgcolor={styles.bggreen}
          icon={styles.icon3}
          chart={styles.chart}
        />

        <SmallBox
          count={5.355}
          title='Task Completed'
          color={styles.orange}
          bgcolor={styles.bgorange}
          icon={styles.icon4}
          chart={styles.chart}
        />
      <div className={styles.clearfix}></div>
      </div>
      <div className={styles.clearfix}></div>
    </div>
  );
}

HomeView.defaultProps = {
  styles: defaultStyles,
}

HomeView.propTypes = {
  styles: PropTypes.object,
}

export default HomeView;
