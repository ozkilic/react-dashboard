import React, { PropTypes } from 'react';
import UserCard from '../../components/UserCard';
import Navigation from '../../components/Navigation';

import defaultStyles from './styles.css';

const data = {
  name: "Oğuz",
  surname: "KILIÇ",
  title: "Software Developer",
  photo:"https://scontent-frt3-1.xx.fbcdn.net/v/t1.0-1/p100x100/13912876_10154373152019840_2912261759851466746_n.jpg?oh=17367301247e288ed8d6118a027a1f01&oe=5905B5A2"
}

function SideBar({ styles }) {
  return (
    <div className={styles.sidebarContainer}>
      <UserCard {...data} />
      <Navigation />
    </div>
  );
}

SideBar.defaultProps = {
  styles: defaultStyles,
};

SideBar.propTypes = {
  styles: PropTypes.object,
};

export default SideBar;
