import React, { Component, PropTypes } from 'react';
import defaultStyles from './styles.css';

export class MasterLayout extends Component {
  constructor(props) {
    super(props);
    this.renderMain = this.renderMain.bind(this);
    this.renderHeader = this.renderHeader.bind(this);
  }

  renderMain() {
    const { children, styles } = this.props;
    return (
      <div className={styles.mainContainer}>
        {children}
      </div>
    );
  }

  renderHeader() {
    const { header, styles } = this.props;
    return (
      <div className={styles.headerContainer}>
        {header}
      </div>
    );
  }

  render() {
    const { renderMain, renderHeader } = this;
    const { styles } = this.props;
    return (
      <div className={styles.wrapper}>
        {renderHeader()}
        {renderMain()}
      </div>
    );
  }
}

MasterLayout.defaultProps = {
  styles: defaultStyles,
}

MasterLayout.propTypes = {
  children: PropTypes.node.isRequired,
  header: PropTypes.node.isRequired,
  styles: PropTypes.object,
};

export default MasterLayout;
