import React, { Component, PropTypes } from 'react';
import Header from '../../containers/Header';
import Footer from '../../components/Footer';
import MasterLayout from '../Master';

class SingleColumnLayout extends Component {
  constructor(props) {
    super(props);
    this.renderHeader = this.renderHeader.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.renderMain = this.renderMain.bind(this);
  }

  renderHeader() {
    return [
      <Header key="header" />,
    ];
  }

  renderFooter() {
    return <Footer />;
  }

  renderMain(main) {
    return (
      <div class="main">
        {main}
      </div>
    );
  }

  render() {
    const { renderMain, renderHeader, renderFooter } = this;
    const { main } = this.props;
    return (
      <MasterLayout header={renderHeader()} footer={renderFooter()}>
        {renderMain(main)}
      </MasterLayout>
    );
  }
}

SingleColumnLayout.propTypes = {
  main: PropTypes.element.isRequired,
};

export default SingleColumnLayout;
