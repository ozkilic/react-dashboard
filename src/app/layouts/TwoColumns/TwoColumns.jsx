import React, { PropTypes } from 'react';
import styles from './styles.css';
import MasterLayout from '../Master';
import SingleColumn from '../SingleColumn';

class TwoColumnsLayout extends SingleColumn {
  renderMain(main, sidebar, footer) {
    return (
      <div className={styles.container}>
        <div className={styles.sidebar}>
          {sidebar}
        </div>
        <div className={styles.main}>
          {main}
          {footer}
        </div>
      </div>
    );
  }
  render() {
    const { renderMain, renderHeader } = this;
    const { sidebar, main, footer } = this.props;
    return (
      <MasterLayout header={renderHeader()}>
        {renderMain(main, sidebar, footer)}
      </MasterLayout>
    );
  }
}

TwoColumnsLayout.propTypes = {
  main: PropTypes.element.isRequired,
  footer: PropTypes.element.isRequired,
  sidebar: PropTypes.element.isRequired,
};

export default TwoColumnsLayout;
